#pragma once
class Core
{
public:
	Core() = default;
	~Core();

	static void Tests();
	static void Start();
	static void LoadNeural();
};

