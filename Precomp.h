#pragma once

#include <algorithm>
#include <chrono>
#include <fstream>
#include <future>
#include <sstream>
#include <string>
#include <vector>

#include <mnist/mnist_reader.hpp>

using namespace std::chrono;

template <typename T>
inline std::ostream& operator << (std::ostream& os, const std::vector<T>& v) {
	if (!v.empty()) {
		os << v[0];
		for (size_t i = 1; i < v.size(); ++i)
			os << "," << v[i];
	}
	return os;
}