#pragma once

#include "Network.hpp"

class NetworkLoader
{
public:
	static void Save(std::shared_ptr<NeuralNetwork> pEngine, const std::string& FilePath);

	static std::shared_ptr<DigitRecogniser> Load(const std::string& FilePath);
};

