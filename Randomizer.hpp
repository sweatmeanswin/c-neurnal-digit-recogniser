#pragma once

#include <random>

class Randomizer
{
public:
	explicit Randomizer(float d = 1.0f) : delim(sqrt(d)) {
		std::random_device rd;
		mt.seed(rd());
	}
	~Randomizer() = default;

	Randomizer(const Randomizer& rhs) = delete;
	Randomizer& operator = (const Randomizer& rhs) = delete;
	
	float operator() () {
		return dist(mt) / delim;
	}
private:
	float delim;
	std::mt19937 mt;
	std::normal_distribution<float> dist;
};
