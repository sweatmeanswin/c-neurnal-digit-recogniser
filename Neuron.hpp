#pragma once

#include "Math.hpp"

#include <vector>
#include <cassert>

using std::vector;

struct Neuron
{
	vector<float> Inputs;
	float Threshold;

	float Process(const vector<float>& InputData) const {
		assert(InputData.size() == Inputs.size());

		float sum{ -Threshold };
		for (size_t j = 0; j < Inputs.size(); ++j) {
			sum += InputData[j] * Inputs[j];
		}
		return Sigmoid(sum);
	}
};
