#include "Precomp.h"

#include "TeacherFabric.hpp"

#include "Math.hpp"

#include <iomanip>

TeacherFabric::TeacherFabric(
	size_t InputDataSize,
	TestNodeVector TrainData,
	TestNodeVector TestData
) : m_inputSize(InputDataSize), m_trainD(TrainData), m_testD(TestData)
{

}

int ID = 1;

std::mutex mx;

int get_id() {
	std::lock_guard<std::mutex> g(mx);
	return ID++;
}

void log(std::string s) {
	std::lock_guard<std::mutex> g(mx);
	std::cout << s.c_str() << std::endl;
}

std::future<std::shared_ptr<LearnStatistic>> TeacherFabric::Run(const std::shared_ptr<LearnParams> Params) const
{
	return std::async(&TeacherFabric::teach, this, Params);
}

std::future<float> TeacherFabric::Check(const std::shared_ptr<NeuralNetwork> Rec, const TestNodeVector & TestData)
{
	return std::async(&TeacherFabric::check, Rec, std::ref(TestData));
}

float TeacherFabric::check(const std::shared_ptr<NeuralNetwork> Rec, const TestNodeVector & TestData)
{
	size_t positives{};
	for (size_t idx = 0; idx < TestData.size(); ++idx) {
		auto res = Rec->Recognise(TestData[idx].Data);
		if (res.Digit() == TestData[idx].Label)
			++positives;
	}
	return float(positives) / TestData.size();
}

std::shared_ptr<LearnStatistic> TeacherFabric::teach(const std::shared_ptr<LearnParams> pParams) const
{
	auto pResStatistic = std::make_shared<LearnStatistic>();
	pResStatistic->Params = pParams;
	// Prepare output
	int id = get_id();
	std::ostringstream ss;
	ss << id << " - " 
		<< "{" << pParams->Layers 
		<< "/" << pParams->Eta
		<< "/" << pParams->Lambda
		<< "}";
	std::string strId = ss.str();
	ss << " E" << pParams->Epochs << " (" << pParams->BatchSize << ")";
	log(ss.str());

	// Init neural
	auto pRecEngine = std::make_shared<DigitRecogniser>(m_inputSize, pParams->Layers, quadraticDelta);
	pResStatistic->EnginePtr = pRecEngine;
	// Init same size 
	vector<Vector> newBiases(pRecEngine->m_biases);
	vector<Matrix> newWeights;
	for (auto & m : pRecEngine->m_weights) {
		newWeights.emplace_back(m.GetShape());
	}

	auto nKoef{ pParams->Eta / pParams->BatchSize };
	auto lKoef{ 1 - pParams->Eta * (pParams->Lambda / m_trainD.size()) };
	// Copy data to random shuffle every epoch
	auto trainData(m_trainD);
	float prevResult{};

	auto startTS = high_resolution_clock::now();
	// Iterate epochs
	for (size_t eIdx = 0; eIdx < pParams->Epochs; ++eIdx)
	{
		// Shuffle for learning
		std::random_shuffle(trainData.begin(), trainData.end());

		// Iterate batches
		for (size_t i = 0; i < trainData.size(); i += pParams->BatchSize)
		{
			// Process one mini batch
			for (size_t j = i; j < i + pParams->BatchSize; ++j)
			{
				auto delta = pRecEngine->backpropagation(m_trainD[j].Data, m_trainD[j].Label);
				auto& deltaBiases = delta.first;
				auto& deltaWeights = delta.second;
				// Update
				for (size_t k = 0; k < deltaBiases.size(); ++k) {
					newWeights[k] += deltaWeights[k];
					newBiases[k] += deltaBiases[k];
				}
			}
			// Update weights
			for (size_t j = 0; j < newWeights.size(); ++j) {
				// (1-eta*(lmbda/n))*w-(eta/len(mini_batch))*nw
				pRecEngine->m_weights[j] = pRecEngine->m_weights[j] * lKoef - newWeights[j] * nKoef;
				// Clear matrix for next iteration
				newWeights[j].Fill(0.0f);
			}
			// Update bias
			for (size_t j = 0; j < newBiases.size(); ++j) {
				pRecEngine->m_biases[j] -= newBiases[j] * nKoef;
				// Clear vector for next iteration
				std::fill(newBiases[j].begin(), newBiases[j].end(), 0.0f);
			}
		}
		// Success rate
		float curResult = check(pRecEngine, m_testD);
		pResStatistic->SuccessHistory.push_back(curResult);
		// Output result
		std::ostringstream ss;
		ss << eIdx + 1 << "\tepoch" 
			<< std::setprecision(4) << std::fixed 
			<< "(" << curResult << ") "
			<< (curResult - prevResult >= 0 ? "+" : "") 
			<< curResult - prevResult 
			<< "\t" << strId;
		log(ss.str());
		// Remember to sum diff
		prevResult = curResult;
	}
	pResStatistic->NetworkLearnTime =
		duration_cast<seconds>(high_resolution_clock::now() - startTS).count();
	return pResStatistic;
}
