#pragma once

#include "VectorMath.hpp"

struct Shape
{
	Shape(size_t rows, size_t columns) : m_rows(rows), m_columns(columns) {}

	size_t Rows() const {
		return m_rows;
	}
	size_t Columns() const {
		return m_columns;
	}
	Shape GetReversed() const {
		return Shape(m_columns, m_rows);
	}
	void Reverse() {
		std::swap(m_rows, m_columns);
	}

	bool operator == (const Shape& rhs) const {
		return m_rows == rhs.m_rows && m_columns == rhs.m_columns;
	}
	bool operator != (const Shape& rhs) const {
		return !(this->operator==(rhs));
	}

private:
	size_t m_rows;
	size_t m_columns;
};

template <typename T>
class BaseMatrix
{
public:
	using BaseVector = std::vector<T>;

	explicit BaseMatrix() : m_shape(0, 0)
	{
	}

	explicit BaseMatrix(size_t rows, size_t columns) : BaseMatrix(Shape{ rows, columns })
	{
	}

	explicit BaseMatrix(Shape shape)
		: m_shape(std::move(shape)), m_data(shape.Rows(), BaseVector(shape.Columns()))
	{
	}

	explicit BaseMatrix(BaseVector vec) : m_shape(1, vec.size()), m_data(1)
	{
		m_data[0] = std::move(vec);
	}

	explicit BaseMatrix(std::vector<BaseVector> vecs) : m_shape(vecs.size(), vecs[0].size()) {
		for (BaseVector & bv : vecs) {
			if (bv.size() != m_shape.Columns())
				throw std::exception();
		}
		m_data = std::move(vecs);
	}

	~BaseMatrix() = default;

	Shape GetShape() const {
		return m_shape;
	}

	BaseVector ToVector() const {
		if (m_shape.Rows() == 1) {
			return m_data[0];
		}
		else if (m_shape.Columns() == 1) {
			return this->Transpose().ToVector();
		}
		throw std::exception();
	}

	BaseMatrix Transpose() const {
		BaseMatrix mat(m_shape.GetReversed());
		for (size_t i = 0; i < m_shape.Rows(); ++i) {
			for (size_t j = 0; j < m_shape.Columns(); ++j) {
				mat.m_data[j][i] = m_data[i][j];
			}
		}
		return mat;
	}

	static BaseMatrix& Transpose(BaseMatrix&& mat) {
		mat.m_shape.Reverse();
		vector<BaseVector> newData(mat.m_shape.Rows(), BaseVector(mat.m_shape.Columns()));

		for (size_t i = 0; i < mat.m_shape.Rows(); ++i) {
			for (size_t j = 0; j < mat.m_shape.Columns(); ++j) {
				newData[i][j] = mat.m_data[j][i];
			}
		}
		mat.m_data = std::move(newData);
		return mat;
	}

	template <typename F>
	BaseMatrix Apply(F func) const {
		BaseMatrix copy(*this);
		for (size_t i = 0; i < m_shape.Rows(); ++i) {
			for (size_t j = 0; j < m_shape.Columns(); ++j) {
				copy.m_data[i][j] = func(copy.m_data[i][j]);
			}
		}
		return copy;
	}

	void Fill(T value) {
		for (size_t i = 0; i < m_shape.Rows(); ++i) {
			std::fill(m_data[i].begin(), m_data[i].end(), value);
		}
	}

	BaseMatrix HadamarProduct(const BaseMatrix& rhs) const {
		assertShape(rhs);

		BaseMatrix copy(*this);
		for (size_t i = 0; i < m_shape.Rows(); ++i)
			for (size_t j = 0; j < m_shape.Columns(); ++j)
				copy.m_data[i][j] *= rhs.m_data[i][j];
		return copy;
	}

	BaseMatrix operator + (const T& value) const {
		BaseMatrix copy(*this);
		for (size_t i = 0; i < m_shape.Rows(); ++i) {
			copy.m_data[i] = copy.m_data[i] + value;
		}
		return copy;
	}

	BaseMatrix operator - (const T& value) const {
		BaseMatrix copy(*this);
		for (size_t i = 0; i < m_shape.Rows(); ++i) {
			copy.m_data[i] = copy.m_data[i] - value;
		}
		return copy;
	}

	BaseMatrix operator * (const T& value) const {
		BaseMatrix copy(*this);
		for (size_t i = 0; i < m_shape.Rows(); ++i) {
			copy.m_data[i] = copy.m_data[i] * value;
		}
		return copy;
	}

	BaseMatrix operator + (const BaseMatrix& rhs) const {
		assertShape(rhs.GetShape());

		BaseMatrix copy(*this);
		for (size_t i = 0; i < m_shape.Rows(); ++i)
			for (size_t j = 0; j < m_shape.Columns(); ++j)
				copy.m_data[i][j] += rhs.m_data[i][j];
		return copy;
	}

	BaseMatrix operator - (const BaseMatrix& rhs) const {
		assertShape(rhs.GetShape());

		BaseMatrix copy(*this);
		for (size_t i = 0; i < m_shape.Rows(); ++i)
			for (size_t j = 0; j < m_shape.Columns(); ++j)
				copy.m_data[i][j] -= rhs.m_data[i][j];
		return copy;
	}

	BaseMatrix operator * (const BaseMatrix& rhs) const {
		if (m_shape.Columns() != rhs.GetShape().Rows())
			throw std::exception();

		BaseMatrix mat(m_shape.Rows(), rhs.m_shape.Columns());
		for (size_t i = 0; i < mat.GetShape().Rows(); ++i) {
			for (size_t j = 0; j < mat.GetShape().Columns(); ++j) {
				for (size_t k = 0; k < m_shape.Columns(); ++k) {
					mat.m_data[i][j] += m_data[i][k] * rhs.m_data[k][j];
				}
			}
		}
		return mat;
	}

	BaseMatrix& operator += (const BaseMatrix& rhs) {
		assertShape(rhs.GetShape());

		for (size_t i = 0; i < m_shape.Rows(); ++i)
			for (size_t j = 0; j < m_shape.Columns(); ++j)
				m_data[i][j] += rhs.m_data[i][j];
		return *this;
	}

	BaseMatrix& operator -= (const BaseMatrix& rhs) {
		assertShape(rhs.GetShape());

		for (size_t i = 0; i < m_shape.Rows(); ++i)
			for (size_t j = 0; j < m_shape.Columns(); ++j)
				m_data[i][j] -= rhs.m_data[i][j];
		return *this;
	}

	BaseMatrix operator * (const BaseVector& rhs) const {
		if (m_shape.Rows() != 1 && m_shape.Columns() != rhs.size())
			throw std::exception();

		// Vector[X,1] * Vector[1,Y] -> Matrix[X,Y]
		if (m_shape.Rows() == 1) {
			BaseMatrix mat(m_shape.Columns(), rhs.size());
			for (size_t i = 0; i < m_shape.Columns(); ++i) {
				for (size_t j = 0; j < rhs.size(); ++j) {
					mat.m_data[i][j] = m_data[0][i] * rhs[j];
				}
			}
			return mat;
		}
		else {
			// Matrix[X,Y] * Vector[Y,1] -> Vector[X,1]
			BaseMatrix mat(1, m_shape.Rows());
			for (size_t i = 0; i < m_shape.Rows(); ++i)
				mat.m_data[0][i] = m_data[i] * rhs;
			return mat;
		}
	}

	const BaseVector& operator [] (size_t rowIndex) const {
		if (rowIndex < 0 || rowIndex >= m_shape.Rows())
			throw std::exception();
		return m_data[rowIndex];
	}

	bool operator == (const BaseMatrix& mat) const {
		if (m_shape != mat.GetShape())
			return false;
		for (size_t i = 0; i < m_shape.Rows(); ++i) {
			if (m_data[i] != mat.m_data[i])
				return false;
		}
		return true;
	}

	bool operator != (const BaseMatrix& mat) const {
		return !(this->operator==(mat));
	}

	operator std::vector<BaseVector>() const {
		return m_data;
	}

private:
	Shape m_shape;
	std::vector<BaseVector> m_data;

	void assertShape(const Shape& sh) const {
		if (m_shape != sh)
			throw std::exception();
	}
};
