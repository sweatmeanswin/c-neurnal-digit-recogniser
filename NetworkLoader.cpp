#include "Precomp.h"

#include "NetworkLoader.hpp"

template <typename T>
void writeLine(std::ofstream& fstr, const T& v) {
	fstr << v << std::endl;
}

void writeVectorLine(std::ofstream& fstr, const Vector& vec) {
	fstr << vec.size() << std::endl;
	fstr.write(reinterpret_cast<const char*>(&vec[0]), vec.size() * (sizeof(float) / sizeof(char)));
	fstr << std::endl;
}

void readVectorLine(std::ifstream& fstr, Vector& vec) {
	// Read vector
	size_t vecSize{};
	fstr >> vecSize;
	fstr.ignore(64, '\n');
	vec.resize(vecSize);
	fstr.read(reinterpret_cast<char*>(&vec[0]), vecSize * (sizeof(float) / sizeof(char)));
}

void NetworkLoader::Save(std::shared_ptr<NeuralNetwork> pEngine, const std::string & FilePath)
{
	std::ofstream fstr(FilePath, std::ios::binary);

	writeLine(fstr, pEngine->m_biases.size());
	for (const Vector& vec : pEngine->m_biases) {
		writeVectorLine(fstr, vec);
	}

	for (auto & mat : pEngine->m_weights) {
		writeLine(fstr, mat.GetShape().Rows());
		for (const Vector& vec : vector<Vector>(mat)) {
			writeVectorLine(fstr, vec);
		}
	}
}

std::shared_ptr<DigitRecogniser> NetworkLoader::Load(const std::string & FilePath)
{
	size_t layerCount{}, vecSize{};
	Vector vec;
	vector<char> buffer;
	vector<Vector> biases;
	vector<Matrix> weights;
	vector<size_t> neuronsPerLayer;

	std::ifstream fstr(FilePath, std::ios::binary);
	/*std::string s;
	std::getline(fstr, s);*/
	// Layer count
	fstr >> layerCount;
	// Read all biases
	for (size_t i = 0; i < layerCount; ++i) {
		readVectorLine(fstr, vec);
		if (i < layerCount - 1)
			neuronsPerLayer.push_back(vec.size());
		biases.emplace_back(std::move(vec));
	}
	// Read all weight matrices
	vector<Vector> mat;
	for (size_t i = 0; i < layerCount; ++i) {
		size_t rowCount{};
		fstr >> rowCount;
		for (size_t j = 0; j < rowCount; ++j) {
			readVectorLine(fstr, vec);
			mat.emplace_back(std::move(vec));
		}
		weights.emplace_back(std::move(mat));
	}

	auto pEngine = std::make_shared<DigitRecogniser>(28 * 28, neuronsPerLayer, crossEntropyDelta);
	pEngine->m_biases = std::move(biases);
	pEngine->m_weights = std::move(weights);

	return pEngine;
}
