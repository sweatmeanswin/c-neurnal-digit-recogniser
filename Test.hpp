#pragma once

#include "Math.hpp"

#include <iostream>

std::ostream& operator << (std::ostream& os, const Matrix& m) {
	const Shape& sh = m.GetShape();
	if (sh.Rows() * sh.Columns() > 0) {
		os << "Matrix{" << std::endl;
		for (size_t i = 0; i < sh.Rows(); ++i) {
			os << m[i] << std::endl;
		}
		os << "}";
	}
	return os;
}


class Test
{
public:
	static void VectorMath() {
		Vector v1{ 1, 2, 3, 4, 5 };
		show(v1);
		show(v1 * 2);
		show(v1 * 0);
		show(v1 + 1);
		show(v1 - 1);
		Vector v2{ 1 };
		show(v2);
		Vector v3{ 9, 8, 7, 6, 5 };
		show(v1 * v3);
		show(v1 + v3);
		show(v1 - v3);
	}

	static void MatrixMath() {
		Matrix m1{ {Vector{1, 2}, Vector{3, 4}} };
		show(m1);
		show(m1 * 2);
		show(m1 + 1);
		show(m1 - 1);
		Matrix m2{ {Vector{0, 0}, Vector{2, 2}} };
		show(m1 + m2);
		show(m1 * m2);
		show(m1 - m2);
		m1 += m2;
		show(m1);
		m1 -= m2 * 2;
		show(m1);
		Matrix m3{ {Vector{1, 2, 3}} };
		Vector v3{ 0, 5 };
		show(m3 * v3);
	}

private:
	template <typename T>
	static void show(T&& t) {
		// std::cout << t << std::endl;
	}

	void assert(const Matrix&& m1, const Matrix&& m2) {
		show(m1);
		if (m1 != m2) throw std::exception();
	}
};
