#include "Precomp.h"

#include "Network.hpp"

const size_t DIGITS_COUNT = 10;

DigitRecogniser::DigitRecogniser(size_t InputLayer, vector<size_t> NeuronsPerLayers, DeltaFunction DeltaF)
	: NeuralNetwork(InputLayer, DIGITS_COUNT, std::move(NeuronsPerLayers)), m_outputs(DIGITS_COUNT, Vector(DIGITS_COUNT)), m_deltaFunc(DeltaF)
{
	// Create output vectors
	for (uint8_t digit = 0; digit < DIGITS_COUNT; ++digit)
		m_outputs[digit][digit] = 1.0f;
}

const Vector & DigitRecogniser::OutputExpectation(uint8_t Digit) const
{
	return m_outputs[Digit];
}

std::pair < std::vector<Vector>, std::vector<Matrix >> DigitRecogniser::backpropagation(const Vector & Input, uint8_t Label)
{
	std::vector<Vector> newBiases(m_biases.size());
	std::vector<Matrix> newWeights(m_weights.size());
	// Feedforward
	Vector activation = Input;
	std::vector<Vector> activations({ Input });
	std::vector<Vector> zs(m_layerCount);

	for (size_t i = 0; i < m_layerCount; ++i) {
		zs[i] = (m_weights[i] * activation).ToVector() + m_biases[i];
		activation = Transform(zs[i], Sigmoid);
		activations.push_back(activation);
	}

	Vector delta = m_deltaFunc(activation, m_outputs[Label], zs[zs.size() - 1]);
	newBiases[newBiases.size() - 1] = delta;
	newWeights[newWeights.size() - 1] = Matrix::Transpose(Matrix(activations[activations.size() - 2]) * delta);

	for (size_t l = 2; l <= m_layerCount; ++l) {
		delta = HadamarProduct(
			(m_weights[m_weights.size() - l + 1].Transpose() * delta).ToVector(),
			Transform(zs[zs.size() - l], SigmoidPrime)
		);
		newBiases[newBiases.size() - l] = delta;
		newWeights[newWeights.size() - l] = Matrix::Transpose(Matrix(activations[activations.size() - l - 1]) * delta);
	}
	return std::make_pair(std::move(newBiases), std::move(newWeights));
}
