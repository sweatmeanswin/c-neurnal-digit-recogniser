#pragma once

#include "MatrixMath.hpp"

using Matrix = BaseMatrix<float>;
using Vector = Matrix::BaseVector;

struct RecogniseResult
{
	RecogniseResult(Vector Data) :
		m_output(std::move(Data)),
		m_digit(std::distance(m_output.cbegin(), std::max_element(m_output.cbegin(), m_output.cend()))) {
	}

	Vector OutputVector() const {
		return m_output;
	}
	size_t Digit() const {
		return m_digit;
	}

private:
	Vector m_output;
	size_t m_digit;
};

class NeuralNetwork
{
public:
	NeuralNetwork(size_t InputLayer, size_t OutputLayer, vector<size_t> NeuronsPerLayers);

	virtual RecogniseResult Recognise(const Vector& InputData) const;
protected:
	vector<Vector> m_biases;
	vector<Matrix> m_weights;
	size_t m_layerCount;

	void init(const vector<size_t>& neuronsPerLayers);

	friend class NetworkLoader;
};
