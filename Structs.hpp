#pragma once

#include "Base.hpp"

struct LearnParams
{
	std::vector<size_t> Layers;
	size_t BatchSize;
	size_t Epochs;
	float Lambda;
	float Eta;
};

struct TimeExecution
{
	float Max;
	float Min;
	float Avg;
};

struct LearnStatistic
{
	std::shared_ptr<NeuralNetwork> EnginePtr;
	std::shared_ptr<LearnParams> Params;
	std::vector<float> SuccessHistory;
	size_t NetworkLearnTime;
};

struct TestNode
{
	Vector Data;
	uint8_t Label;
};
using TestNodeVector = vector<TestNode>;

inline TestNodeVector ToNVec(const vector<Vector>& data, const vector<uint8_t>& labels) {
	TestNodeVector vec;
	vec.reserve(data.size());
	for (size_t i = 0; i < data.size(); ++i) {
		vec.emplace_back(std::move(TestNode{ data[i], labels[i] }));
	}
	return vec;
}