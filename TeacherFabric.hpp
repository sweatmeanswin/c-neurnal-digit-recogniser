#pragma once

#include "Structs.hpp"
#include "Network.hpp"

class TeacherFabric
{
public:
	TeacherFabric(
		size_t InputDataSize,
		TestNodeVector TrainData,
		TestNodeVector TestData
	);
	~TeacherFabric() = default;

	std::future<std::shared_ptr<LearnStatistic>> Run(const std::shared_ptr<LearnParams> Params) const;

	static std::future<float> Check(
		const std::shared_ptr<NeuralNetwork> Rec,
		const TestNodeVector& TestData
	);

protected:
	size_t m_inputSize;
	TestNodeVector m_trainD;
	TestNodeVector m_testD;

	std::shared_ptr<LearnStatistic> teach(const std::shared_ptr<LearnParams> pParams) const;

	static float check(const std::shared_ptr<NeuralNetwork> Rec, const TestNodeVector& TestData);
};
