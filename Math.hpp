#pragma once

#include <cmath>

inline float Sigmoid(float x)
{
	return 1.0f / (1.0f + std::exp(-x));
}

inline float SigmoidPrime(float x)
{
	return Sigmoid(x) * (1 - Sigmoid(x));
}
