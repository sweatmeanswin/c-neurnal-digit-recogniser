#pragma once

#include "Base.hpp"
#include "Math.hpp"

using DeltaFunction = std::function<Vector(Vector, const Vector&, const Vector&)>;

static auto quadraticDelta = DeltaFunction([](Vector a, const Vector& y, const Vector& x) { return HadamarProduct(a - y, Transform(x, SigmoidPrime)); });
static auto crossEntropyDelta = DeltaFunction([](Vector a, const Vector& y, const Vector& x) { return a - y; });

class DigitRecogniser : public NeuralNetwork
{
public:
	DigitRecogniser(size_t InputLayer, vector<size_t> NeuronsPerLayers, DeltaFunction DeltaF);
	~DigitRecogniser() = default;
	
	const Vector& OutputExpectation(uint8_t Digit) const;
private:
	vector<Vector> m_outputs;
	DeltaFunction m_deltaFunc;

	std::pair<std::vector<Vector>, vector<Matrix>> backpropagation(const Vector& Data, uint8_t Label);

	friend class NetworkLoader;
	friend class TeacherFabric;
};
