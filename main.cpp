#include "Precomp.h"

#include "Core.hpp"

int main()
{
	Core().Start();
	Core().LoadNeural();
	return 0;
}
