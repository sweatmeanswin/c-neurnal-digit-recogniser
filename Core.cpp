#include "Precomp.h"

#include "Core.hpp"

#include "TeacherFabric.hpp"
#include "Test.hpp"
#include "NetworkLoader.hpp"

const std::string MNIST_PATH = "D:/Projects/C++/NeuralDigitRecogniser/Data";

struct LearnEngineInfo
{
	std::future<std::shared_ptr<LearnStatistic>> fLResult;
	std::future<float> fSuccess;
	std::shared_ptr<LearnStatistic> LStat;
	std::shared_ptr<LearnParams> LParams;
	size_t TestRecCount;
	float Success;
};

Core::~Core()
{
	std::cout << "Finish" << std::endl;
	std::string s;
	std::getline(std::cin, s);
}

void Core::Tests()
{
	Test::VectorMath();
	Test::MatrixMath();
}

std::string ToString(std::shared_ptr<LearnParams> pParams) {
	std::ostringstream ss;
	ss << "L[" << pParams->Layers
		<< "]_ETA[" << pParams->Eta
		<< "]_LMB[" << pParams->Lambda
		<< "]_E[" << pParams->Epochs
		<< "]_Batch[" << pParams->BatchSize << "]";
	return ss.str();
}

void AppendStatistic(LearnEngineInfo& eInfo) {
	auto params = eInfo.LStat->Params;
	{
		std::ofstream res("output.txt", std::ios::app);
		res << ToString(params)
			<< ", Success(" << eInfo.Success << ")"
			<< " from [" << eInfo.TestRecCount << "]"
			<< std::endl
			<< "T[" << eInfo.LStat->NetworkLearnTime
			<< "]_AE[" << static_cast<float>(eInfo.LStat->NetworkLearnTime) / params->Epochs << "]"
			<< std::endl
			<< "History[" << eInfo.LStat->SuccessHistory << "]"
			<< std::endl;
	}
	{
		std::ofstream res("output.csv", std::ios::app);
		res << params->Layers
			<< ":" << params->Eta
			<< ":" << params->Lambda
			<< ":" << params->Epochs
			<< ":" << params->BatchSize
			<< ":" << eInfo.Success
			<< ":" << eInfo.TestRecCount
			<< ":" << eInfo.LStat->NetworkLearnTime
			<< ":" << static_cast<float>(eInfo.LStat->NetworkLearnTime) / params->Epochs
			<< ":" << eInfo.LStat->SuccessHistory
			<< std::endl;
	}
}

struct MNISTData
{
	TestNodeVector Train;
	TestNodeVector Validation;
	TestNodeVector Test;
};

MNISTData loadMNISTData() {
	MNISTData res;
	// Load MNIST
	auto dataset = mnist::read_dataset_direct<vector, Vector, uint8_t>(MNIST_PATH);
	// Train data
	res.Train = ToNVec(dataset.training_images, dataset.training_labels);
	// Last 10k for validation
	std::move(res.Train.begin() + 50000, res.Train.end(), std::back_inserter(res.Validation));
	res.Train.erase(res.Train.cbegin() + 50000, res.Train.cend());
	// Test data
	res.Test = ToNVec(dataset.test_images, dataset.test_labels);
	return res;
}

void Core::Start()
{
	auto data = loadMNISTData();

	// Init fabric
	TeacherFabric fab(28 * 28, data.Train, data.Validation);
	std::vector<LearnEngineInfo> EngineInfo;

	const size_t EPOCHS = 15;
	const size_t BATCHS_SIZE = 10;
	const float Eta = 0.01f;
	/// Iterate different neuron counts
	for (vector<size_t>& v : vector<vector<size_t>>{ {100}, {50} /*, {30, 15}*/ }) {
		for (float l : {1.0f, 0.1f}) {
			LearnEngineInfo s;
			s.LParams = std::make_shared<LearnParams>();
			s.LParams->Layers = v;
			s.LParams->Eta = Eta;
			s.LParams->Lambda = l;
			s.LParams->BatchSize = BATCHS_SIZE;
			s.LParams->Epochs = EPOCHS;
			s.fLResult = fab.Run(s.LParams);
			EngineInfo.emplace_back(std::move(s));
		}
	}

	for (auto& s : EngineInfo) {
		s.LStat = s.fLResult.get();
		s.fSuccess = TeacherFabric::Check(s.LStat->EnginePtr, data.Test);
		s.TestRecCount = data.Test.size();
	}

	for (auto& s : EngineInfo) {
		s.Success = s.fSuccess.get();
		std::cout << s.Success << std::endl;
		if (s.Success > 0.8f) {
			NetworkLoader::Save(s.LStat->EnginePtr, "neural/" + ToString(s.LParams));
		}
		AppendStatistic(s);
	}
}

void Core::LoadNeural()
{
	auto data = loadMNISTData();

	const std::string dir("D:/Projects/C++/NeuralDigitRecogniser/NeuralDigitRecogniser/neural/");
	const std::string name("L[100]_ETA[0.01]_E[10]_Batch[10]");
	auto pEngine = NetworkLoader::Load(dir + name);
	size_t positives{}, offset = 1500;
	for (auto it = data.Test.cbegin(); it != data.Test.cbegin() + offset; ++it) {
		auto res = pEngine->Recognise(it->Data);
		if (res.Digit() == it->Label)
			++positives;
	}
	std::cout << float(positives) / offset << std::endl;
}
