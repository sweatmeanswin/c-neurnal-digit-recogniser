#include "Precomp.h"

#include "Base.hpp"

#include "Math.hpp"
#include "Randomizer.hpp"

NeuralNetwork::NeuralNetwork(size_t InputLayer, size_t OutputLayer, vector<size_t> NeuronsPerLayers)
	: m_layerCount(NeuronsPerLayers.size() + 1)
{
	// Add input layer
	NeuronsPerLayers.insert(NeuronsPerLayers.begin(), InputLayer);
	// Add output layer
	NeuronsPerLayers.push_back(OutputLayer);
	// Create neurons
	init(NeuronsPerLayers);
}

RecogniseResult NeuralNetwork::Recognise(const Vector& InputData) const
{
	Vector dataVector(InputData);
	for (size_t i = 0; i < m_weights.size(); ++i) {
		dataVector = Transform(
			(m_weights[i] * dataVector).ToVector() + m_biases[i],
			Sigmoid
		);
	}
	return RecogniseResult(dataVector);
}

void NeuralNetwork::init(const vector<size_t>& neuronsPerLayers)
{
	Randomizer biasesRnd;
	// Hidden
	auto fit = neuronsPerLayers.cbegin(), sit = neuronsPerLayers.cbegin() + 1;
	while (sit != neuronsPerLayers.cend())
	{
		// Init *sit neurons (current layer) with weights for *fit neurons count (previous layer)
		std::vector<Vector> wv(*sit, Vector(*fit));
		// For each vector generate random values
		// Init Gaussian random (0 mean, 1 deviation)/sqrt(last layer neuron count) for weights
		Randomizer rnd(static_cast<float>(*fit));
		std::for_each(wv.begin(), wv.end(), [&rnd](Vector& v) { std::generate(v.begin(), v.end(), std::ref(rnd)); });
		m_weights.emplace_back(std::move(wv));
		// Biases
		Vector bv(*sit);
		std::generate(bv.begin(), bv.end(), std::ref(biasesRnd));
		m_biases.emplace_back(std::move(bv));
		// Inc
		++fit;
		++sit;
	}
}
