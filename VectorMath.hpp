#pragma once

#include <vector>

using std::vector;

template <typename T>
inline void assertSize(const vector<T>& lhs, const vector<T>& rhs) {
	if (lhs.size() != rhs.size())
		throw std::exception();
}

template <typename T>
inline float operator * (const vector<T>& lhs, const vector<T>& rhs) {
	assertSize(lhs, rhs);

	float s{};
	for (size_t i = 0; i < lhs.size(); ++i) {
		s += lhs[i] * rhs[i];
	}
	return s;
}

template <typename T>
inline vector<T> operator + (vector<T> lhs, float rhs) {
	for (auto & f : lhs)
		f += rhs;
	return lhs;
}

template <typename T>
inline vector<T> operator - (vector<T> lhs, float rhs) {
	for (auto & f : lhs)
		f -= rhs;
	return lhs;
}

template <typename T>
inline vector<T> operator * (vector<T> lhs, float rhs) {
	for (auto & f : lhs)
		f *= rhs;
	return lhs;
}

template <typename T>
inline vector<T> operator *= (vector<T> lhs, float rhs) {
	for (auto & f : lhs)
		f *= rhs;
	return lhs;
}

template <typename T>
inline vector<T> operator + (vector<T> lhs, const vector<T>& rhs) {
	assertSize(lhs, rhs);

	for (size_t i = 0; i < rhs.size(); ++i) {
		lhs[i] += rhs[i];
	}
	return lhs;
}

template <typename T>
inline vector<T> operator - (vector<T> lhs, const vector<T>& rhs) {
	assertSize(lhs, rhs);

	for (size_t i = 0; i < rhs.size(); ++i) {
		lhs[i] -= rhs[i];
	}
	return lhs;
}

template <typename T>
inline vector<T>& operator += (vector<T>& lhs, const vector<T>& rhs) {
	assertSize(lhs, rhs);

	for (size_t i = 0; i < rhs.size(); ++i) {
		lhs[i] += rhs[i];
	}
	return lhs;
}

template <typename T>
inline vector<T>& operator -= (vector<T>& lhs, const vector<T>& rhs) {
	assertSize(lhs, rhs);

	for (size_t i = 0; i < rhs.size(); ++i) {
		lhs[i] += rhs[i];
	}
	return lhs;
}

template <typename T, typename F>
inline vector<T> Transform(vector<T> vec, F func) {
	std::transform(vec.cbegin(), vec.cend(), vec.begin(), func);
	return std::move(vec);
}

template <typename T, typename F>
inline vector<T>& Apply(vector<T>& vec, F func) {
	std::transform(vec.cbegin(), vec.cend(), vec.begin(), func);
	return vec;
}

template <typename T, typename F>
inline vector<T> Apply(vector<T>&& lhs, vector<T>&& rhs, F func) {
	assertSize(lhs, rhs);

	vector<T> res(lhs.size());
	for (size_t i = 0; i < res.size(); ++i) {
		res[i] = func(lhs[i], rhs[i]);
	}
	return res;
}

template <typename T>
inline vector<T> HadamarProduct(vector<T> lhs, const vector<T>& rhs) {
	assertSize(lhs, rhs);

	for (size_t i = 0; i < lhs.size(); ++i)
		lhs[i] *= rhs[i];
	return lhs;
}
